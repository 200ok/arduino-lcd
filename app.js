/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , user = require('./routes/user')
  , http = require('http')
  , path = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/users', user.list);

var server = http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});


var five = require("./node_modules/johnny-five"),
           board, lcd;
var isWin = !!process.platform.match(/^win/);

if (isWin) {
  board = new five.Board({ port: "com5" });
} else {
  board = new five.Board();
}

board.on("ready", function() {

  lcd = new five.LCD({
    pins: [ 8, 9, 4, 5, 6, 7 ],
  });

  backlight = new five.Pin({
    addr: 10
  });


  lcd.on("ready", function() {

    // creates a heart
    lcd.createChar( 0x07,
      [ 0x00, 0x0a, 0x1f, 0x1f, 0x0e, 0x04, 0x00, 0x00 ]
    );

    // // .high() for on, .low() for off
    // backlight.high();
    // // Cursor 0,0 is implied
    // lcd.clear();
    // lcd.setCursor(0, 0);
    // lcd.print("Go Team Random!");
    // // move to second row
    // lcd.setCursor(0, 1);
    // lcd.print("Now node-tastic!");

    // lcd.clear();
    // lcd.setCursor(0, 0);
    // lcd.print("isWin? " + isWin);

    // Lots of hearts...
    // lcd.clear().print("I love node");
    // lcd.cursor(1, 0);
    // for ( var i=0; i<16; i++ ) {
    //     lcd.write(7);
    // };
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("LCD Ready");

  });

  // this.repl.inject({
  //   lcd: lcd
  // });

  function readyDisplay(message) {
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("LCD Ready");
    lcd.setCursor(0,1);
    lcd.print(message);
  }

  function changeTheDisplay(message) {
    if (message.backLight) {
      backlight.high();
    } else {
      backlight.low();
    }
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print(message.topRow);
    lcd.setCursor(0,1);
    lcd.print(message.bottomRow);
  }

  var io = require('socket.io').listen(server);

  io.sockets.on('connection', function(socket) {

      socket.on("changeTheDisplay", function(message){
          console.log("changeTheDisplay - topRow: " + message.topRow + ", bottomRow: " + message.bottomRow +  ", backlight " + message.backLight);
          changeTheDisplay(message);
      });

      socket.on("readyToChange", function(readyMessage){
        console.log(readyMessage);
        // readyDisplay(readyMessage);
      });

  });


});
