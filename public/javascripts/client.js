console.log("Client");

var socket = io.connect("http://localhost:3000");

function changeTheDisplay(val) {
  socket.emit("changeTheDisplay", val);
}

$("#bar").on("click", function(e){
    changeTheDisplay({
        topRow: $('#topRow').val(),
        bottomRow: $('#bottomRow').val(),
        backLight: $('#backLight').prop("checked")
    });
});

$(document).ready(function(){
    socket.emit("readyToChange", "Client Ready");
});